#!/bin/bash

# Install Docker Daemon
sudo yum update -y
sudo yum install -y docker
sudo service docker start
sudo usermod -aG docker ec2-user

# Install Docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# The mount location 
MOUNT_LOCATION="/mnt/efs"

# Mount target 
MOUNT_TARGET=${mount-target-dns}

# Create the directory that will hold the mount
sudo mkdir -p $MOUNT_LOCATION

# Mount the EFS mount point as a NFSv4.1 fs
sudo mount \
    -t nfs4 \
    -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 \
    $MOUNT_TARGET:/ $MOUNT_LOCATION

# Check whether it has been successfuly mounted or not:
df -h | grep efs
mount | grep nfs

