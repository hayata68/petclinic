
# Render a part using a `template_file`
data "template_file" "script" {
  template = "${file("install.tpl")}"

  vars {
    mount-target-dns = "${aws_efs_mount_target.main.0.dns_name}"
  }
}